
package gptintegration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GPTIntegration {

    public static void main(String[] args) {
        
        String apiUrl = "https://api.openai.com/v1/completions";
        String openaiApiKey = "";
        
        try{
            
            URL url = new URL(apiUrl);
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod("POST");
            
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + openaiApiKey);
            
            connection.setDoOutput(true);
            
            String jsonInputString = "{\"model\":\"text-davinci-003\",\"prompt\":\"quien soy yo?\",\"max_tokens\":70,\"temperature\":0.8}";
            
            
            try(OutputStream outputStream = connection.getOutputStream()){
                outputStream.write(jsonInputString.getBytes());
                outputStream.flush();
            
            }
            
            
            StringBuilder response = new StringBuilder();
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
                String inputLine;
                while((inputLine = bufferedReader.readLine()) !=null){
                    response.append(inputLine);
                }
            
            }
            
//            System.out.println(response);

            ObjectMapper objectMapper =  new ObjectMapper();
            JsonNode jsonResponse = objectMapper.readTree(response.toString());
            
//            System.out.println(jsonResponse.get("choices"));  
            
            JsonNode choices =jsonResponse.get("choices");
            
            for (JsonNode choice : choices){
        
                String text = choice.get("text").asText();
                System.out.println(text);
        
        }

            
        
        } catch (Exception e){
        
            System.out.println(e);
        }
        
    }
    
}
